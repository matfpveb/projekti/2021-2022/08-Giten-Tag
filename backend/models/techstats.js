const db = require('./db');

const techstatsSchema = new db.Schema({
    _id: db.Schema.Types.ObjectId,
    numLinesAdded: {
        type: db.Schema.Types.Number,
        default: 0
    },
    numLinesDeleted: {
        type: db.Schema.Types.Number,
        default: 0
    },
    type: {
        type:db.Schema.Types.String,
    },
    tech: {
        type:db.Schema.Types.String,
    },
    numOfCommits: {
        type : db.Schema.Types.Number,
    }
});

const TechStats = db.model('TechStats', techstatsSchema);
module.exports = TechStats;
