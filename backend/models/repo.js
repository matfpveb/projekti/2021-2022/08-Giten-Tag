const db = require('./db');

const repoSchema = new db.Schema({
    _id: db.Schema.Types.ObjectId,
    name: {
        type: db.Schema.Types.String,
    },
    // commits: [{
    //     type: db.Schema.Types.ObjectId,
    //     ref: 'Commit'
    //  }],
    numOfLines: {
        type: db.Schema.Types.Number,
      //  required: true
    },
    numOfCommits: {
        type: db.Schema.Types.Number,
      //  required: true
    },
    user: {
        type: db.Schema.Types.ObjectId,
        ref: 'User'
    },
    mapLangToNumOfCommits: {
        type : db.Schema.Types.Mixed,
    },
    mapLangToNumOfLines: {
        type: db.Schema.Types.Mixed,
    },
    username: {
        type: db.Schema.Types.String
    },
    description: {
        type: db.Schema.Types.String
    },
    url: {
        type: db.Schema.Types.String
    }
});

const Repo = db.model('Repo', repoSchema);
module.exports = Repo;