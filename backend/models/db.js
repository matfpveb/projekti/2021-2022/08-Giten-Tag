const mongoose = require('mongoose');

const databaseString = process.env.DB_STRING || 'mongodb://127.0.0.1:27017/gitentag';

mongoose.connect(databaseString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

mongoose.connection.once('open', function () {
    console.log('Succesfully connected to mongodb');
});

mongoose.connection.on('error', (error) => {
    console.log('Greska: ', error);
});

module.exports = mongoose;