const reposController = require("../controllers/repo");
const fs = require("fs");
const git = require("nodegit");
const util = require("util");
const exec = util.promisify(require('child_process').exec);
const userService = require('../services/users');

const USE_NATIVE_GIT_FOR_CLONE = false;

const user = JSON.parse(process.argv[2]);
const token = process.argv[3];
const repos = JSON.parse(process.argv[4]);

let cloneOptions = {
    fetchOpts: {
        callbacks: {
            credentials: function () {
                return git.Cred.userpassPlaintextNew(token, "x-oauth-basic");
            }
        }
    }
};

let emails = user.githubEmails === undefined ? [] : user.githubEmails;
emails.push(user.email);

(async () => {
    for(const repo of repos) {
        const repoPath = `repos/${user.username}/${repo.name}/`;
        if(fs.existsSync(repoPath)) {
            fs.rmSync(repoPath, { recursive: true, force: true });
        }
        try {
            if(USE_NATIVE_GIT_FOR_CLONE) {
                await exec(`git clone ${repo.clone} ${repoPath}`)
            } else {
                await git.Clone(repo.clone, repoPath, cloneOptions);
            }
            console.log(`Cloned repo ${repo.name} for user ${user.username}. Sending to processing.`)
            await reposController.getAllCommitsInfo2(repoPath, emails, user.username, repo.url, repo.description);
            console.log(`Finished processing data for repo: ${repo.name} for user ${user.username}.`);
            fs.rmSync(repoPath, { recursive: true, force: true });
        } catch (e) {
            console.error(`Error processing repo: ${repo.name} for user ${user.username}: ${e}`);
        }
    }
    try {
        const userModel = await userService.getUserByUsername(user.username);
        userModel.isRepoUpdateInProgress = false;
        await userModel.save();
    } catch (e) {
        console.error(`Error saving user status for ${user.username}: ${e}`);
    }
    process.exit(0);
})();


