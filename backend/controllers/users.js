const users = require('../services/users');
const validator = require('validator');
const User = require('../models/user');
const github = require('../services/github')
const jwt = require("jsonwebtoken");


const registerUser = async (req, res) => {
    const {username, email, password, location, dateOfBirth} = req.body;

    if (!username || !email || !password ||
        !validator.isEmail(email) || !validator.isAlphanumeric(username)) {
        res.status(400).json({message: "One of the fields is not formatted properly."});
    } else {
        try {
            const user = await users.registerUser(username, email, password, location, dateOfBirth);
            res.status(200).json(users.generateToken(user));
        } catch (e) {
            if(e.name === 'MongoServerError' && e.message.indexOf("duplicate") >= 0) {
                res.status(409).json({message: "User with this email or username already exists. Try logging in instead."});
            } else {
                console.error(e);
                res.status(500).json({message: "An unknown error has occurred. Please try again later."});
            }
        }
    }
};

const loginUser = async (req, res) => {
    const {email, password} = req.body;

    if (!email || !password) {
        res.status(400).json({message: "Email or password not provided."});
    } else {
        try {
            const user = await users.loginUser(email, password);
            res.status(200).json(users.generateToken(user));
        } catch (e) {
            if(e.name === 'UnauthenticatedUserException') {
                res.status(401).json({message: "Email or password is incorrect. Please re-check and try again."});
            } else {
                console.error(e);
                res.status(500).json({message: "An unknown error has occurred. Please try again later."});
            }
        }
    }
};

const resetPassword = async (req, res) => {
    const {oldPassword, newPassword} = req.body;
    const email = req.user.sub;
    try {
        const newTokens = await users.resetPassword(email, oldPassword, newPassword)
            .then(user => users.generateToken(user));
        res.status(200).json(newTokens);
    } catch (e) {
        if(e.name === 'UnauthenticatedUserException') {
            res.status(401).json({message: "Old password is incorrect. Please re-check and try again."});
        } else {
            console.error(e);
            res.status(500).json({message: "An unknown error has occurred. Please try again later."});
        }
    }

}

const isTokenRevoked = async(req, payload, done) => {
    const user = await users.getUser(payload.sub);
    if(user == null || (payload.iat+1) * 1000 < user.tokenInvalidBefore) {
        console.log(`User ${payload.sub} attempting to use a revoked token`);
        done({status: 401, message: 'Token is revoked.'}, true);
    } else {
        done(null, false);
    }
}

const refreshToken = async (req, res) => {
    if(req.user.type !== 'refresh') {
        res.status(401).json();
        return;
    }
    const user = await users.getUser(req.user.sub);
    res.status(200).json(users.generateToken(user));
}

const getUserInfo = async (req, res) => {
    let user;
    let showPrivate;
    if(req.query.username !== undefined) {
        showPrivate = false;
        user = await users.getUserByUsername(req.query.username);
        if(!user) {
            res.status(404).json({"code": "USER_NOT_FOUND", "message": "Username not found"});
            return;
        }
    } else {
        showPrivate = true;
        const authHeader = req.header('Authorization');
        if(authHeader == null || !authHeader.includes(' ')) {
            res.status(401).json({code: 400, message: "Username or auth token required"});
            return;
        }
        const token = authHeader.split(' ', 2)[1];
        try {
            const userData = jwt.verify(token, process.env.JWT_SECRET, {});
            user = await users.getUser(userData.sub);
        } catch (e) {
            console.log(`Error decoding token on get user: ${e.message}`);
            res.status(401).json({code: 401, message: e.message});
            return;
        }
    }
    let userResponse = {
        email: user.email,
        username: user.username,
        location: user.location,
        dateOfBirth: user.dateOfBirth,
        githubUrl: user.githubUrl,
        avatar: user.githubAvatarUrl
    };
    for(let field in userResponse) {
        const isPublic = !user.privateFields.includes(field);
        if(!showPrivate && !isPublic) {
            userResponse[field] = undefined;
        } else {
            userResponse[field] = {
                value: userResponse[field],
                show: isPublic && userResponse[field] != null && userResponse[field] !== ''
            }
        }
    }
    userResponse.inProgress = !!user.isRepoUpdateInProgress;
    res.status(200).json(userResponse);
}


const editInfo = async (req, res) => {
    const authHeader = req.header('Authorization');
    if(authHeader == null || !authHeader.includes(' ')) {
        res.status(401).json({code: 400, message: "No jwt token present or token is misformated"});
        return;
    }
    const token = authHeader.split(' ', 2)[1];
    try {
        const userData = jwt.verify(token, process.env.JWT_SECRET, {});
        const user = await users.getUser(userData.sub);
        const {email, location, dateOfBirth} = req.body;
        user.privateFields = [];
        if(location != null) {
            if(location.value != null) {
                user.location = location.value;
            } else {
                user.location = undefined;
            }
            if(!location.show) user.privateFields.push("location");
        }
        if(dateOfBirth != null) {
            if(dateOfBirth.value !== "" && dateOfBirth.value != null) {
                user.dateOfBirth = new Date(dateOfBirth.value);
            } else {
                user.dateOfBirth = undefined;
            }
            if(!dateOfBirth.show) user.privateFields.push("dateOfBirth");
        }
        if(email != null && !email.show) {
            user.privateFields.push("email");
        }
        try {
            await user.save();
        } catch(e) {
            if(e.name === 'MongoServerError' && e.message.indexOf("duplicate") >= 0) {
                res.status(409).json({message: "User with this email or username already exists."});
                return;
            } else {
                console.error(e);
                res.status(500).json({message: "An unknown error has occurred. Please try again later."});
                return;
            }
        }
        res.status(200).json({
            email: user.email,
            username: user.username,
            location: user.location,
            dateOfBirth: user.dateOfBirth,
            githubUrl: user.githubUrl,
            avatar: user.githubAvatarUrl
        })
    } catch (e) {
        console.log(`Error on get user: ${e.message}`);
        res.status(500).json({code: 401, message: "An unknown error has occurred"});
    }
}

const deleteUser = async(req, res) => {
    const authHeader = req.header('Authorization');
    if(authHeader == null || !authHeader.includes(' ')) {
        res.status(401).json({code: 400, message: "No jwt token present or token is misformated"});
        return;
    }
    const token = authHeader.split(' ', 2)[1];
    try {
        const userData = jwt.verify(token, process.env.JWT_SECRET, {});
        const user = await users.getUser(userData.sub);
        await User.findByIdAndDelete(user.id);
        res.status(200).json();
    } catch (e) {
        console.log(`Error on delete user: ${e.message}`);
        res.status(500).json({code: 401, message: "An unknown error has occurred"});
    }
}

module.exports = {
    registerUser,
    loginUser,
    resetPassword,
    refreshToken,
    isTokenRevoked,
    getUserInfo,
    editInfo,
    deleteUser
};
