const GITHUB_LOGIN_URL = "https://github.com/login/oauth/authorize";
const HOST = "https://pzv.luka-j.rocks";
const CALLBACK_PATH = "/api/github/callback";
const AUTH_STATE_SEP = " ";
const CLIENT_ID = process.env.GITHUB_GITENTAG_CLIENT_ID;
const CLIENT_SECRET = process.env.GITHUB_GITENTAG_CLIENT_SECRET;

const jwt = require("jsonwebtoken")
const fetch = require("cross-fetch");
const users = require("../services/users");
const crypto = require("crypto");
const service = require("../services/github");
const repoService = require("../services/repo");
const commitService = require("../services/commit");
const githubService = require("../services/github");
const github = require("../services/github");

const authStates = new Set();


const loginToGithub = async (req, res) => {
    const accessToken = req.query.token;
    if(accessToken === undefined || accessToken === '') {
        res.status(400).json({code: "NO_ACCESS_TOKEN", message: "Access token must be provided as a query param for this operation."});
        return;
    }
    try {
        const userData = jwt.verify(accessToken, process.env.JWT_SECRET, {});
        const email = userData.sub;
        const randomString = crypto.randomBytes(16).toString("hex");
        const state = email + AUTH_STATE_SEP + randomString;
        authStates.add(state);
        res.redirect(GITHUB_LOGIN_URL + "?client_id=" + process.env.GITHUB_GITENTAG_CLIENT_ID + "&redirect_uri=" + HOST +
            CALLBACK_PATH + "&scope=read:user user:email repo&state=" + state);
    } catch (e) {
        res.status(401).json({code: "ILLEGAL_ACCESS_TOKEN", message: "Access token is invalid."});
    }
}

const githubOauth2Callback = async(req, res) => {
    const code = req.query.code;
    const state = req.query.state
    if(!authStates.has(state)) {
        res.status(401).json({code: "INVALID_CODE"});
        return;
    }
    authStates.delete(state);
    const authResponse = await fetch.fetch("https://github.com/login/oauth/access_token?client_id=" + CLIENT_ID +
        "&client_secret=" + CLIENT_SECRET + "&code=" + code, {
        method: "POST",
        headers: {
            Accept: "application/json"
        }
    });
    const authBody = await authResponse.json();
    if(authResponse.status !== 200 || authBody.error !== undefined) {
        console.log(authBody);
        res.status(500);
    } else {
        const stateData = state.split(AUTH_STATE_SEP);
        const userEmail = stateData[0];
        const user = await users.getUser(userEmail);
        if(user == null) {
            console.error("Can't find user for email " + userEmail);
            res.status(500).json();
        } else {
            user.githubToken = authBody.access_token;
            await githubService.updateUserWithGithubData(user);
            res.redirect("https://gitentag.luka-j.rocks/info/" + user.username);
            await github.getRepos(user);
        }
    }
}

const fetchRepos = async(req, res) => {
    const email = req.user.sub;
    const user = await users.getUser(email);
    try {
        if(user.isRepoUpdateInProgress) {
            res.status(409).json({message: "Refresh is already in progress. Please wait...", code: 409});
            return;
        }
        user.isRepoUpdateInProgress = true;
        await user.save();
        await commitService.deleteCommitsForUser(user.username);
        await repoService.deleteReposForUser(user.username);
        await service.getRepos(user, res);
    } catch (e) {
        user.isRepoUpdateInProgress = false;
        try {
            await user.save();
        } catch (e) {
            console.error(`Error saving user data for ${user.username}: ${e}`);
        }
        if(!res.headersSent) {
            console.error(e);
            if (e.name === "UnauthenticatedUserException") {
                res.status(401).json({code: "UNAUTHENTICATED", message: "User not logged in to GitHub"});
            } else {
                res.status(500).json({code: "SERVER_ERROR"});
            }
        }
    }
}


module.exports = {
    loginToGithub,
    githubOauth2Callback,
    fetchRepos
}
