const Repo = require('../models/repo');
const repoServices = require('../services/repo');
const commit = require('../services/commit');
const git = require('nodegit');
var mongoose = require('mongoose');
const users = require('../services/users')
const guessLang = require('../services/guesslang');

//otvara repo preko patha, vraca Repository
const openRepo = async (repoPath) => {
    const repo = await git.Repository.open(repoPath);
    return repo;
}

const getAllCommitsInfo2 = async (path, email, username, url, description) => {
    const repo = await openRepo(path);
    returnValueFromGuessLang = await guessLang.langOfFiles(path);
    const mapPathToLang = returnValueFromGuessLang[1];
    let mapLangToNumOfLines = {};
    console.log(mapPathToLang);
    console.log('------------------------------------------------')
    console.log(mapLangToNumOfLines);
    const allCommits = await getAllCommits(repo, email, username);
    var tmpArray = path.split('/');
    console.debug(tmpArray);
    var repoName = tmpArray[tmpArray.length - 2];
    var mapLangToNumOfCommits = {};
    var localMap = {};
    var username;
    for (commitInstance of allCommits) {
        localMap = await commit.commitInfo(path, repoName, commitInstance, username, email, mapPathToLang);
        for (key in localMap) {
            if (!mapLangToNumOfLines.hasOwnProperty(key))
                mapLangToNumOfLines[key] = localMap[key];
            else
                mapLangToNumOfLines[key] += localMap[key];
            if (!mapLangToNumOfCommits.hasOwnProperty(key))
                mapLangToNumOfCommits[key] = 1;
            else
                mapLangToNumOfCommits[key] += 1;
        }
    }
    await repoServices.addRepo(path, mapLangToNumOfLines, mapLangToNumOfCommits, username, description, url, repoName);
    return allCommits;
}

const getAllCommitsInfo = async (req, res) => {
    const allCommits = await getAllCommitsInfo2('../../../11-SobaZabave/', 'bogdanis799@gmail.com', 'bogdanis', 'urltest', 'descriptiontest');
    res.status(200).json(allCommits);
}

const getAllCommits = async (repo, email, username) => {
    let allCommits = [];
    const repoWalker = git.Revwalk.create(repo);
    repoWalker.pushGlob('refs/heads/*');
    allCommits = await repoWalker.getCommitsUntil(c => true);
    //   const allUserCommits = allCommits.filter(commit => commit.author().toString().split(' ')[commit.author().toString().split(' ').length - 1] == ('<' + email + '>'));
    return allCommits;
}

const getBasicUserData = async (req, res) => {
    const username = req.query.username;
    if (!username) {
        res.status(400).json({ message: "Username not provided." });
    } else {
        try {
            const numberOfRepos = await repoServices.userNumberOfRepos(username);
            const numberOfCommits = await commit.userNumberOfCommits(username);
            const numberOfLines = await commit.userNumberOfLines(username);
            res.status(200).json({ numberOfRepos, numberOfCommits, numberOfLines });
        } catch (e) {
            console.error(e);
            res.status(500).json({ message: "An unknown error has occurred. Please try again later." });
        }
    }
}

// vraca broj koji predstavlja dan u nedelji (1-7)
const getUserMostProductiveDay = async (req, res) => {
    const username = req.query.username;
    if (!username) {
        res.status(400).json({ message: "Username not provided." });
    } else {
        try {
            const allDates = await commit.getAllCommitDates(username);
            var mapDayToNumCommits = {};
            var i;
            for (i = 1; i <= 7; i++) {
                mapDayToNumCommits[i] = 0;
            }

            var maxDay = 1;
            console.debug(mapDayToNumCommits[maxDay]);
            for (date of allDates) {
                const day = Number((date['date']).getDay());
                console.debug(day);
                mapDayToNumCommits[day] += 1;
                console.debug(mapDayToNumCommits)
                if (mapDayToNumCommits[maxDay] < mapDayToNumCommits[day]) {
                    maxDay = day;
                }
            }

            res.status(200).json(maxDay);
        } catch (e) {
            console.error(e);
            res.status(500).json({ message: "An unknown error has occurred. Please try again later." });
        }
    }
}

const getUserMostProductivePeriod = async (req, res) => {
    const username = req.query.username;
    if (!username) {
        res.status(400).json({ message: "Username not provided." });
    } else {
        try {
            const allDates = await commit.getAllCommitDates(username);
            var mapDayPeriodToCommits = {};

            mapDayPeriodToCommits['night'] = 0;
            mapDayPeriodToCommits['morning'] = 0;
            mapDayPeriodToCommits['daytime'] = 0;
            mapDayPeriodToCommits['evening'] = 0;

            var maxPeriod = 'morning';

            for (date of allDates) {
                const hours = Number((date['date']).getHours());
                console.debug(hours);
                let period = 'night';
                if (hours >= 0 && hours < 6) {
                    period = 'night';
                }
                if (hours >= 12 && hours < 18) {
                    period = 'daytime';
                }
                else if (hours >= 18 && hours <= 23) {
                    period = 'evening';
                }

                mapDayPeriodToCommits[period] += 1;

                if (mapDayPeriodToCommits[period] > mapDayPeriodToCommits[maxPeriod]) {
                    maxPeriod = period;
                }
            }

            res.status(200).json(maxPeriod);
        } catch (e) {
            console.error(e);
            res.status(500).json({ message: "An unknown error has occurred. Please try again later." });
        }
    }
}


const getAllUserRepoObjects = async (req, res) => {
    const username = req.query.username;
    if (!username) {
        res.status(400).json({ message: "Username not provided." });
    } else {
        try {
            const allUserRepos = await repoServices.getAllUserRepos(username);
            const result = [];
            const numberOfRepos = allUserRepos.length;
            for (let i = 0; i < numberOfRepos; i++) {
                result[i] = {};
                result[i]['name'] = allUserRepos[i]['name'];

                const mapLangToNumOfRepos = allUserRepos[i]['mapLangToNumOfCommits'];
                let numberOfCommits = 0; //TODO: realan broj linija i komitova
                let numberOfLines = 0;
                let allUsedLanguages = [];
                for (key in mapLangToNumOfRepos) {
                    numberOfCommits += mapLangToNumOfRepos[key];
                    allUsedLanguages.push(key);
                }
                const mapLangToNumOfLines = allUserRepos[i]['mapLangToNumOfLines'];
                for(let lang in mapLangToNumOfLines) numberOfLines += mapLangToNumOfLines[lang]
                result[i]['numOfCommits'] = numberOfCommits;
                result[i]['description'] = allUserRepos[i]['description'];
                result[i]['url'] = allUserRepos[i]['url'];
                result[i]['numOfLines'] = numberOfLines;
                result[i]['languages'] = allUsedLanguages;

            }
            res.status(200).json(result);
        } catch (e) {
            console.error(e);
            res.status(500).json({ message: "An unknown error has occurred. Please try again later." });
        }
    }
};

const getLangNumLines = async (username) => {
    try {
        const languageObjects = await repoServices.getAllUsedLanguagesLines(username);
        const langToLines = {};
        for (let languageObject of languageObjects) {
            const tmp = languageObject['mapLangToNumOfLines'];
            for (language in tmp) {
                if (!langToLines.hasOwnProperty(language)) {
                    langToLines[language] = tmp[language];
                }
                else {
                    langToLines[language] += tmp[language];
                }
            }
        }

        return langToLines;
    } catch (e) {
        console.error(e);
    }
};

const getLangNumCommits = async (username) => {
    try {
        const languageObjects = await repoServices.getAllUsedLanguagesCommits(username);
        const langToCommits = {};
        for (let languageObject of languageObjects) {
            const tmp = languageObject['mapLangToNumOfCommits'];
            for (language in tmp) {
                if (!langToCommits.hasOwnProperty(language)) {
                    langToCommits[language] = tmp[language];
                }
                else {
                    langToCommits[language] += tmp[language];
                }
            }
        }
        console.debug(langToCommits);
        return langToCommits;
    } catch (e) {
        console.error(e);
    }
};

const getLangPercentage = async (username) => {

    try {
        const langLines = await getLangNumLines(username);
        let totalLines = 0;
        for (let ll in langLines) {
            totalLines += langLines[ll];
        }

        langPercentage = {};
        for (let ll in langLines) {
            langPercentage[ll] = 100 * langLines[ll] / totalLines;
        }

        return langPercentage;

    } catch (e) {
        console.error(e);
    }
};

const getLangInfo = async (req, res) => {
    const username = req.query.username;
    const mapLangIdToPic = {
        'C++': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/cplusplus/cplusplus-original.svg',
        'C': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/c/c-original.svg',
        'Python': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/python/python-original.svg',
        'JavaScript': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg',
        'Kotlin': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/kotlin/kotlin-original.svg',
        'CoffeeScript': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/coffeescript/coffeescript-original.svg',
        'TypeScript': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/typescript/typescript-original.svg',
        'Julia': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/julia/julia-original.svg',
        'Ruby': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/ruby/ruby-original.svg',
        'Haskell': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/haskell/haskell-original.svg',
        'Rust': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/rust/rust-plain.svg',
        'Perl': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/perl/perl-original.svg',
        'Jupyter': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/jupyter/jupyter-original.svg',
        'Erlang': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/erlang/erlang-original.svg',
        'Bash': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bash/bash-original.svg',
        'HTML': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg',
        'CSS': 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg',
        'XML': 'https://img.icons8.com/material-outlined/96/000000/gear--v1.png',
        'YAML': 'https://img.icons8.com/material-outlined/96/000000/gear--v1.png',
        'INI': 'https://img.icons8.com/material-outlined/96/000000/gear--v1.png',
        'JSON': 'https://img.icons8.com/material-outlined/96/000000/gear--v1.png',
        'GO' : 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/go/go-original.svg',
        'JAVA' : 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/java/java-original.svg',
        'GROOVY' : 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/groovy/groovy-original.svg',
        'DART' : 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/dart/dart-original-wordmark.svg',
        'Erlang' : 'https://cdn.jsdelivr.net/gh/devicons/devicon/icons/erlang/erlang-original.svg'
    }
    if (!username) {
        res.status(400).json({ message: "Username not provided." });
    } else {
        try {
            const langLines = await getLangNumLines(username);
            const langCommits = await getLangNumCommits(username);
            const langPercentage = await getLangPercentage(username);
            const finalResult = {};
            for (lang in langPercentage) {
                if (!mapLangIdToPic.hasOwnProperty(lang))
                    mapLangIdToPic[lang] = 'https://cdn-icons-png.flaticon.com/512/711/711284.png';
                finalResult[lang] = [langLines[lang], langCommits[lang], langPercentage[lang], mapLangIdToPic[lang]];
            }
            
            res.status(200).json(finalResult);

        } catch (e) {
            console.error(e);
            res.status(500).json({ message: "An unknown error has occurred. Please try again later." });
        }
    }
}


module.exports = {
    getAllCommitsInfo,
    getAllCommitsInfo2,
    getBasicUserData,
    getUserMostProductiveDay,
    getAllUserRepoObjects,
    getUserMostProductivePeriod,
    getLangNumLines,
    getLangPercentage,
    getLangInfo,
    getLangNumCommits
}