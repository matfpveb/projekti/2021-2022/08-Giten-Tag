const express = require('express');
const controller = require('../../controllers/github');

const router = express.Router();

router.get('/login', controller.loginToGithub);
router.get('/callback', controller.githubOauth2Callback);
router.post('/fetch', controller.fetchRepos);

module.exports = router;
