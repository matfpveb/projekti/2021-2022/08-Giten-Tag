const mongoose = require('mongoose');
const Commit = require('../models/commit');
// const TechStats = require('../models/techstats');
const { getDiff, patches, newFile, path, lineStats } = require('nodegit');
const guesslang = require('../services/guesslang');
const { resolve } = require('path');

const commitInfo = async (repoPath, filename, commit, username_, email, mapPathToLang) => {
    var localLangMap = {};
    // autor
    const author = await commit.author();
    var len = author.toString().length;
    // mail
    const array = author.toString().split(' ');
    const arrayLen = array.length;
    const username = username_;
    let mail = array[arrayLen - 1];
    mail = mail.substr(1,mail.length - 2);
    if (!email.includes(mail)) {
        return {};
    }
    // datum
    var date = await commit.date();
    const isMergeCommit = commit.parents().length > 1;

    const diffs = await commit.getDiff();

    var totalLinesAdded = 0;
    for (const diff of diffs) {
        const patches = await diff.patches();
        var i = 0;
        while (patches[i] !== undefined) {
            const diffFile = await patches[i].newFile();
            var path = await diffFile.path();
            path = resolve(repoPath + path);
            let linesAdded;
            if(isMergeCommit) linesAdded = 0;
            else linesAdded = await patches[i].lineStats()['total_additions'];
            if (mapPathToLang.hasOwnProperty(path)) {
                if (!localLangMap.hasOwnProperty(mapPathToLang[path]))
                    localLangMap[mapPathToLang[path]] = linesAdded;
                else
                    localLangMap[mapPathToLang[path]] += linesAdded;
            }
            totalLinesAdded += linesAdded;
            i += 1;
        }
    }
    
    await addCommit(filename, date, mail, totalLinesAdded, username);
    return localLangMap;
};

const addCommit = async (filename, date, mail, totalAdded, username) => {
    const commit = new Commit({
        _id: new mongoose.Types.ObjectId(),
        repoName: filename,
        date: date,
        mail: mail,
        numLinesAdded: totalAdded,
        numLinesDeleted: 0,
        username: username
    });

    await commit.save();
}

const deleteCommitsForUser = async(username) => {
    return Commit.deleteMany({username: username}).exec();
}

const userNumberOfCommits = async (username) => {
    return Commit.countDocuments({ username: username }).exec();
};

const userNumberOfLines = async (username1) => {
    const res = await Commit.aggregate([
        { $match: { username: username1 } },
        { $group: { _id: null, amount: { $sum: "$numLinesAdded" } } }
    ]).exec();
    if(res[0] == null) return 0;
    return res[0]['amount'];
};

const getAllCommitDates = async (username) => {
    return await Commit.find({ username: username }).select('date -_id').exec();
}



module.exports = { commitInfo, userNumberOfCommits, userNumberOfLines, getAllCommitDates, deleteCommitsForUser };
