const {UnauthenticatedUserException} = require("./users");
const fetch = require("cross-fetch");
const { fork } = require("child_process");


const getUserInfo = async(user) => {
    const token = user.githubToken;
    if (token === undefined) {
        throw new UnauthenticatedUserException("User not logged in to GitHub!");
    }

    const response = await fetch.fetch('https://api.github.com/user', {
        method: 'GET',
        headers: {
            Authorization: 'Bearer ' + token
        }
    });
    if (response.status !== 200) {
        console.error("Got non-200 response while fetching github user: " + response);
        throw new NetworkCommunicationError("Failed to get user info");
    }
    const body = await response.json();
    return body
}

const getUserEmails = async(user) => {
    const token = user.githubToken;
    if (token === undefined) {
        throw new UnauthenticatedUserException("User not logged in to GitHub!");
    }

    const response = await fetch.fetch('https://api.github.com/user/emails', {
        method: 'GET',
        headers: {
            Authorization: 'Bearer ' + token
        }
    });
    if (response.status !== 200) {
        console.error("Got non-200 response while fetching github user email: " + response);
        throw new NetworkCommunicationError("Failed to get user email");
    }
    const body = await response.json();
    return body.map(e => e.email);
}

const getRepos = async (user, res) => {
    user.isRepoUpdateInProgress = true;
    await user.save();
    const token = user.githubToken;
    if (token === undefined) {
        throw new UnauthenticatedUserException("User not logged in to GitHub!");
    }

    let repos = [];
    let pageNum = 1;
    while (true) {
        const response = await fetch.fetch('https://api.github.com/user/repos?page=' + pageNum, {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + token
            }
        });
        if (response.status !== 200) {
            console.error("Got non-200 response while fetching github repos: " + response);
            throw new NetworkCommunicationError("Failed to get user repos");
        }
        const body = await response.json();
        if (body.length === 0) break;
        for (let i = 0; i < body.length; i++) {
            const repo = body[i];
            if(!repo["private"]) {
                repos.push({name: repo["name"], clone: repo["clone_url"], description: repo["description"], url: repo["html_url"]});
            } else {
                repos.push({name: "PRIVATE", clone: repo["clone_url"], description: "This repo is private. No details are available.", url: ""});
            }
        }
        pageNum++;
    }

    console.log(`Downloaded ${repos.length} repos for user ${user.username}: ${repos.map(r => r.name)}`);
    if(res !== undefined) {
        res.status(202).json({status: "ACCEPTED"});
    }

    try {
        await updateUserWithGithubData(user);
    } catch (e) {
        console.error(`Error occurred while updating user with latest info from GitHub: ${e}`);
    }

    const userData = JSON.stringify(user);
    const reposData = JSON.stringify(repos);
    try {
        const childProcess = fork('executors/runDataProcessing.js', [userData, token, reposData], {cwd: process.cwd()});
        childProcess.on("close", (code) => {
            console.log(`Finished processing all data for user ${user.username}. Exit code: ${code}`);
        });
    } catch (e) {
        console.error(e);
    }
}

class NetworkCommunicationError extends Error {
    constructor(message) {
        super(message);
        this.name = "NetworkCommunicationError";
    }
}

const updateUserWithGithubData = async(user) => {
    const githubUserInfo = await getUserInfo(user);
    user.githubUrl = githubUserInfo.html_url;
    user.githubAvatarUrl = githubUserInfo.avatar_url;
    user.githubEmails = await getUserEmails(user);
    await user.save();
}

module.exports = {
    getRepos,
    getUserInfo,
    getUserEmails,
    updateUserWithGithubData,
    NetworkCommunicationError
}