# Project Giten-Tag
## <http://gitentag.luka-j.rocks/>

Veb aplikacija koja korisnicima omogućava da naprave nalog koji će služiti kao prezentacija njihovog dosadašnjeg rada na GitHubu. Aplikacija povlači podatke sa GitHub naloga korisnika, i na osnovu komitova koje je napravio, gradi profil koji sadrži kratak pregled istorije rada, najčešće korišćene programske jezike, repozitorijume kojima je korisnik doprinosio, i slične statistike o korisnikovom rada. Profil se gradi jedanput, pri registraciji, i osvežava se periodično ili na zahtev korisnika. Korisnik može da izmeni ili sakrije lične informacije na profilu (npr ime, lokaciju). Profili su javno dostupni.

## Uputstva za izgradnju i pokretanje
Dostupna na [wikiju](https://gitlab.com/matfpveb/projekti/2021-2022/08-Giten-Tag/-/wikis/Uputstva-za-izgradnju-i-pokretanje).

## Demo snimak

Dostupan na <https://youtu.be/pFCaVI-yMrY>.

## Developers

- [Andrijana Aleksić, 99/2018](https://gitlab.com/qndrijana)
- [Aleksandra Pešić, 15/2018](https://gitlab.com/caca20)
- [Bogdan Marković, 130/2018](https://gitlab.com/bogdanis)
- [Jelena Keljać, 106/2018](https://gitlab.com/jkeljac)
- [Luka Jovičić, 38/2018](https://gitlab.com/luka-j)
