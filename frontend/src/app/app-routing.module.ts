import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainPageComponent } from './components/main-page/main-page.component';
import { SettingsComponent } from './components/settings/settings.component';
import { LoginComponent } from './components/user/login/login.component';
import { LogoutComponent } from './components/user/logout/logout.component';
import { RegisterComponent } from './components/user/register/register.component';
import { UserGuardGuard } from './guards/user-guard.guard';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { LoginGuard } from './guards/login.guard';

const routes: Routes = [
  {path:'', component:LoginComponent,canActivate:[LoginGuard]},
  {path:'info/:username', component: MainPageComponent,canActivate:[UserGuardGuard]},
  {path:'settings', component: SettingsComponent,canActivate:[UserGuardGuard]},
  {path:'login', component:LoginComponent,canActivate:[LoginGuard]},
  {path:'register', component:RegisterComponent,canActivate:[LoginGuard]},
  {path: 'logout', component:LogoutComponent},
  {path: '404', component:NotfoundComponent},
  {path: '**', component:NotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
