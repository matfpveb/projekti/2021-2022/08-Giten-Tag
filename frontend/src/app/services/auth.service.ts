import { HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { User } from '../models/user';
import { JwtService } from './jwt.service';
import { Tokens } from '../models/tokens';
import { lastValueFrom } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly urls = {
    loginUrl: environment.backendSecond+ "api/users/login",
    registerUrl:  environment.backendSecond+ "api/users/register",
    getUserUrl:  environment.backendSecond+ "api/users" 
  }

  private readonly userSubject:Subject<User | null> = new Subject<User | null>();
  public readonly user: Observable<User | null > = this.userSubject.asObservable();
  public dateChecked: boolean = false;
  public locationChecked: boolean = false;
  public emailChecked: boolean = false;

  public inProgress: boolean = false;
  private userLoggedIn: boolean = false;

  public get isUserLoggedIn(): boolean {
    return this.userLoggedIn;
  }
  public set isUserLoggedIn(value: boolean) {
    this.userLoggedIn = value;
  }

  constructor(private http:HttpClient, private jwt:JwtService,private router: Router ) {
  }

  public getUser():Observable< User | null > {
    const headers :HttpHeaders = new HttpHeaders().set('content-type', 'application/json').set('Authorization',"Bearer "+this.jwt.getToken());
    const obs: Observable<any> = this.http.get<any>(this.urls.getUserUrl, { 'headers': headers,responseType:"json" });
    
    return obs.pipe(
      map((data: any) => {
        if(data===null){
          return null;
        }
        this.inProgress = data.inProgress;
        this.dateChecked = data.dateOfBirth.show;
        this.emailChecked = data.email.show;
        this.locationChecked = data.location.show;
        const newUser: User =  new User( data.username.value, data.email.value, data.location.value, data.dateOfBirth.value,data.githubUrl.value,data.avatar.value);
        this.userSubject.next(newUser);

        return newUser;
      })
    );
  }

  async registerUser(username:string,email:string, password:string, location:string, dateOfBirth:Date):Promise<any>{
    const body = {username,email,password,location,dateOfBirth};
    const headers = new HttpHeaders().set("Content-Type","application/json");
    return await lastValueFrom(this.http.post<Tokens>(this.urls.registerUrl, body,{headers,responseType: "json"}).pipe(
      catchError((error: HttpErrorResponse) => this.handleError(error))
    ));
  }

  async login(email: string, password: string): Promise<any> {
    const body = {
      email,
      password,
    };
    return await lastValueFrom(this.http.post<Tokens>(this.urls.loginUrl, body, {responseType: "json"}).pipe(
      catchError((error: HttpErrorResponse) => this.handleError(error))
    ));
  }

  public logoutUser(): void {
    this.jwt.removeToken();
    this.userSubject.next(null);
    this.isUserLoggedIn = false;
    localStorage.removeItem("username")
  }

  public handleError(error: HttpErrorResponse): any {
    const message = error.error.message
    const status = error.status
    window.alert(`There was an error: ${message}. Server returned code: ${status}`)
    return ""
  }

  public handleErrorMainPage(error: HttpErrorResponse): any {
    const message = error.error.message
    const status = error.status
    window.alert(`There was an error: ${message}. Server returned code: ${status}`)

    return "404"
  }
}
