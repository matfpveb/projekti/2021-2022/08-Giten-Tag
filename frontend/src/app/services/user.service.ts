
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { JwtService } from './jwt.service';
import { catchError, lastValueFrom } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly urls = {
    usersUrl: environment.backendSecond+ "api/users",
    changePasswordUrl:  environment.backendSecond+ "api/users/password/reset",
  }

  constructor(private http: HttpClient, private jwt: JwtService) {}


  public async changePassword(oldPassword:string,newPassword:string): Promise<any>{
    const body={oldPassword,newPassword};
    const headers: HttpHeaders = new HttpHeaders().set("Authorization", `Bearer ${this.jwt.getToken()}`);
    return await lastValueFrom( this.http.post<any>(this.urls.changePasswordUrl,body,{headers}).pipe(
      catchError((error: HttpErrorResponse) => this.handleError(error))
    ));
  }

  public async changeUserInfo(showEmail: boolean,location:string, showLocation: boolean ,dateOfBirth:string,showDate: boolean){
    if(dateOfBirth==="undefined"){
      dateOfBirth="";
    }
    const body = {"email" : {
                    "show": showEmail
                },
                "location":{
                    "value":location,
                    "show":showLocation
                },
                "dateOfBirth":{
                    "value":dateOfBirth,
                    "show":showDate
                }};
    const headers: HttpHeaders = new HttpHeaders().set("Authorization", `Bearer ${this.jwt.getToken()}`);
    return await lastValueFrom(this.http.put<any>(this.urls.usersUrl,body,{headers}).pipe(
      catchError((error: HttpErrorResponse) => this.handleError(error))
    ))
  }

  public handleError(error: HttpErrorResponse): any {
    const message = error.error.message
    const status = error.status
    window.alert(`There was an error: ${message}. Server returned code: ${status}`)
    return ""
  }
  
}
