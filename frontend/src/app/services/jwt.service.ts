import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  private static readonly TOKENID: string = "JWT_TOKEN";

  constructor() { }

  public setToken(jwt:string){
    localStorage.setItem(JwtService.TOKENID,jwt);
  }

  public getToken():string{
    const token:string | null = localStorage.getItem(JwtService.TOKENID);
    if(!token){
      return ' ';
    }
    return token;
  }

  public removeToken():void{
    localStorage.removeItem(JwtService.TOKENID);
  }
}
