import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './common/nav-bar/nav-bar.component';
import { MainPageComponent } from './components/main-page/main-page.component';
import { SettingsComponent } from './components/settings/settings.component';
import { AboutComponent } from './components/main-page/about/about.component';
import { LanguagesComponent } from './components/main-page/languages/languages.component';
import { StatisticsComponent } from './components/main-page/statistics/statistics.component';
import { ProjectsComponent } from './components/main-page/projects/projects.component';
import { UserInfoComponent } from './components/main-page/user-info/user-info.component';
import { BoxComponent } from './components/main-page/languages/box/box.component';
import { LoginComponent } from './components/user/login/login.component';
import { RegisterComponent } from './components/user/register/register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LogoutComponent } from './components/user/logout/logout.component';
import { NotfoundComponent } from './components/notfound/notfound.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    MainPageComponent,
    SettingsComponent,
    AboutComponent,
    LanguagesComponent,
    StatisticsComponent,
    ProjectsComponent,
    UserInfoComponent,
    BoxComponent,
    LoginComponent,
    RegisterComponent,
    LogoutComponent,
    NotfoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
