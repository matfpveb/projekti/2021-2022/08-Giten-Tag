import { Component, OnInit } from '@angular/core';
import { lastValueFrom, Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { JwtService } from 'src/app/services/jwt.service';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  sub:Subscription = new Subscription();
  user:User | null=null;
  public dateString:string|undefined=undefined;
  public visible : boolean = true;  


  changeInfo:boolean = false;
  changePass:boolean = false;
  userForm:FormGroup;
  passwordForm:FormGroup;

  isDateChecked:boolean =false;
  isLocationChecked:boolean=false;
  isEmailChecked:boolean =false;

  inProgress: boolean = false;

  private readonly urls = {
    deleteAccountUrl: environment.backendSecond+"api/users",
    refreshUrl:environment.backendSecond+"api/github/fetch"
  }
  
  constructor(private auth:AuthService, private router:Router, private userService:UserService, private jwt:JwtService, private http:HttpClient) {
    this.sub = auth.user.subscribe((user:User | null)=>{
      this.user=user;
    });
    this.isDateChecked = this.auth.dateChecked;
    this.isLocationChecked = this.auth.locationChecked;
    this.isEmailChecked = this.auth.emailChecked;

    this.dateString =  this.user?.dateOfBirth?.getUTCMonth()+"/"+this.user?.dateOfBirth?.getUTCDate()+"/"+this.user?.dateOfBirth?.getFullYear();

    this.userForm = new FormGroup({
      username: new FormControl("", [Validators.required, Validators.pattern(new RegExp("[a-zA-Z0-9_-]{4,}"))]),
      email: new FormControl("", [Validators.required, Validators.email]),
      location: new FormControl("", [Validators.minLength(2)]),
      dateOfBirth: new FormControl(new Date(""))
    });

    this.passwordForm = new FormGroup({
      oldPass: new FormControl("",[Validators.required,Validators.minLength(6)]), 
      newPass: new FormControl("",[Validators.required,Validators.minLength(6)])
    });
  }


  onChangeCheckDate():void{
    this.isDateChecked = !this.isDateChecked;
  }

  onChangeCheckEmail():void{
    this.isEmailChecked= !this.isEmailChecked;
  }

  onChangeCheckLocation():void{
    this.isLocationChecked=!this.isLocationChecked;
  }

  async ngOnInit(): Promise<void> {

    this.user = await lastValueFrom( this.auth.getUser());
    this.visible = false;  
    this.inProgress = this.auth.inProgress;

    const date = String(this.user?.dateOfBirth).substring(0,10)
    this.userForm = new FormGroup({
      username: new FormControl(this.user?.username, [Validators.required, Validators.pattern(new RegExp("[a-zA-Z0-9_-]{4,}"))]),
      email: new FormControl(this.user?.email, [Validators.required, Validators.email]),
      location: new FormControl(this.user?.location, [ Validators.minLength(2)]),
      dateOfBirth: new FormControl(date)
    });

    this.isDateChecked = this.auth.dateChecked;
    this.isLocationChecked = this.auth.locationChecked;
    this.isEmailChecked = this.auth.emailChecked;

  }

  onChangeInfo(){
    this.changeInfo=true;
  }

  async onSubmitPass():Promise<void>{
    this.changePass = false;
    if(this.passwordForm.valid){
      let oldP = this.passwordForm.value.oldPass;
      let newP = this.passwordForm.value.newPass;
      let response:any = await this.userService.changePassword(oldP,newP);
      this.jwt.setToken(response.accessToken);

      this.auth.isUserLoggedIn = true;
      const putanja:string =`/info/${this.user?.username}`;
      this.router.navigateByUrl(putanja);

    }
    else{
      window.alert('Form is not valid!');
      return;
    }
  }

  onChangePass(){
    this.changePass=true;
  }

  async onFormSubmit(){
    if (this.userForm.invalid) {
      window.alert('Form is not valid!');
      return;
    } 
    this.changeInfo=false;

    const data = this.userForm.value;

    const response:any = await this.userService.changeUserInfo(this.isEmailChecked,data.location,this.isLocationChecked,data.dateOfBirth,this.isDateChecked)
    
    const obs:Observable<User | null> = this.auth.getUser();
    this.sub = obs.subscribe((user:User|null) =>{
      this.user = user;
      this.changeInfo = false;
      this.auth.isUserLoggedIn = true;

      const putanja:string =`/info/${user?.username}`;
      this.router.navigateByUrl(putanja);
    });
  }


  onCancelChanges(){
    this.changeInfo=false;
    this.userForm = new FormGroup({
      username: new FormControl(this.user?.username, [Validators.required, Validators.pattern(new RegExp("[a-zA-Z0-9_-]{8,}"))]),
      email: new FormControl(this.user?.email, [Validators.required, Validators.email]),
      location: new FormControl(this.user?.location, [Validators.minLength(2)]),
      dateOfBirth: new FormControl( this.user?.dateOfBirth)
    });

  }

  async confirmDelete():Promise<void>{
    if(confirm("Are you sure to delete your account?")){
      const headers :HttpHeaders = new HttpHeaders().set('Authorization',"Bearer "+this.jwt.getToken()).set("Content-Type","application/json");
    this.auth.logoutUser();
    await lastValueFrom( this.http.delete(this.urls.deleteAccountUrl,{headers}));
    this.router.navigateByUrl('/login');
    }
  }

  onCancelPass(){
    this.changePass = false;

    this.passwordForm = new FormGroup({
      oldPass: new FormControl("",[Validators.required]),
      newPass: new FormControl("",[Validators.required])
    });
  }

  async onRefresh(){
    const headers :HttpHeaders = new HttpHeaders().set('Authorization',"Bearer "+this.jwt.getToken()).set("Content-Type","application/json");
    await lastValueFrom(this.http.post(this.urls.refreshUrl,{},{headers}));
    this.router.navigateByUrl('/info/'+this.user?.username);
  }
}
