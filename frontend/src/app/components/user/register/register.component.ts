import { Component, Inject, OnDestroy, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";
import { Observable, Subscription } from "rxjs";
import { User } from "src/app/models/user";
import { JwtService } from "src/app/services/jwt.service";
import { DOCUMENT } from "@angular/common";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  registerForm: FormGroup;
  registerSub: Subscription = new Subscription();

  private readonly urls = {
    refreshUrl:environment.backendSecond+"api/gitfub/fetch"
  }

  constructor(@Inject(DOCUMENT) private document:Document, private authService: AuthService, private router:Router, private jwt:JwtService, private http:HttpClient) { 
    this.registerForm = new FormGroup({
      username: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_-]{4,}$/)]),
      password: new FormControl("", [Validators.required, Validators.minLength(8)]),
      email: new FormControl("", [Validators.required, Validators.email]),
      location: new FormControl("",[Validators.minLength(2)]),
      dateOfBirth:new FormControl("",)
    });
   }

  public ngOnInit(): void {
      
  }

  public ngOnDestroy(): void {
    this.registerSub ? this.registerSub.unsubscribe() : null;
  }

  public emptyRegisterFields():void{
    this.registerForm = new FormGroup({
      username: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_-]{4,}$/)]),
      password: new FormControl("", [Validators.required, Validators.minLength(8)]),
      email: new FormControl("", [Validators.required, Validators.email]),
      location: new FormControl("",[Validators.minLength(2)]),
      dateOfBirth:new FormControl("",)
    });
  }

  async register(): Promise<void> {
    if (this.registerForm.invalid) {
      window.alert("The form is not valid!");
      this.emptyRegisterFields();
      return;
    }

    const data = this.registerForm.value;
    let response:any = await this.authService.registerUser(data.username, data.email, data.password, data.location, data.dateOfBirth);
    if(response === ""){
      this.emptyRegisterFields();
      return;
    }

    this.jwt.setToken(response.accessToken);
    
    const obs:Observable<User | null> = this.authService.getUser();
    
    this.registerSub = obs.subscribe((user:User|null) =>{

      localStorage.setItem('username',String(user?.username))

      this.authService.isUserLoggedIn = true
      this.document.location.href= environment.backendSecond+`api/github/login?token=`+this.jwt.getToken();
      this.router.navigateByUrl('/info/'+data.username);
    })
  }
}
