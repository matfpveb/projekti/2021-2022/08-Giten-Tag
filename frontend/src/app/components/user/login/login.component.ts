import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';
import { Observable } from 'rxjs';
import { JwtService } from 'src/app/services/jwt.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  loginSub: Subscription=new Subscription(); 


  constructor(private auth: AuthService, private router:Router,private jwt:JwtService) {
    this.loginForm = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required, Validators.minLength(6)])
    });
  }

  public ngOnDestroy(): void {
    this.loginSub ? this.loginSub.unsubscribe() : null;
  }

  public ngOnInit(): void {
  }

  async login(): Promise<void> {
    if (this.loginForm.invalid) {
      window.alert('Form is not valid!');
      this.emptyLoginFields();
      return;
    }

    const data = this.loginForm.value;
    let response:any = await this.auth.login(data.email, data.password);
   
    if(response === ""){
      this.emptyLoginFields();
      return;
    }


    this.jwt.setToken(response.accessToken);
    
    const obs: Observable<User|null> = this.auth.getUser();
    this.loginSub = obs.subscribe((user: User|null) => {
      this.auth.isUserLoggedIn = true;

      localStorage.setItem('username',String(user?.username))

      const putanja:string =`/info/${user?.username}`;
      this.router.navigateByUrl(putanja);
    });
  }

  public emptyLoginFields():void{
    this.loginForm = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required, Validators.minLength(6)])
    });
  }
}
