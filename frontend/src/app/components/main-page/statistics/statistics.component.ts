import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Stats } from 'src/app/models/stats';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {

  @Input() statistics:Stats;

  constructor(private http:HttpClient) {
    this.statistics = new Stats();
   }


  ngOnInit(): void {
  }

}
