import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  @Input() public funFacts: Map<string,string> = new Map<string,string>();

  constructor() {}

  ngOnInit(): void {
  }

}
