import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { User } from "src/app/models/user";
import { AuthService } from 'src/app/services/auth.service';
import { JwtService } from 'src/app/services/jwt.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit, OnDestroy {

  public dateString:string = "";

  @Input() user:User|null;

  constructor(private auth: AuthService,private jwt:JwtService,private http: HttpClient) {
    this.user = null;
  }

  ngOnInit(): void {
    this.dateString = String( this.user?.dateOfBirth).substring(0,10);
  }

  public makeStringDate(date:Date|null|undefined):string|null{
    if(date===null || date ===undefined)
      return null;
    var strDate = String(date).substring(0,10);
    return strDate;
  }

  showLocation(location:string|null|undefined):boolean{
    if(location===null || location==="" || location===undefined)
      return false;
    return true;
  }

  showDate(date:Date|null|undefined):boolean{
    if(date===null || date===undefined)
      return false;
    return true;
  }

  ngOnDestroy(){
    
  }

}
