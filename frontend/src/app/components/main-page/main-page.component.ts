import { Component, OnDestroy, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { catchError, lastValueFrom, map, Subscription } from 'rxjs';
import { JwtService } from 'src/app/services/jwt.service';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Stats } from 'src/app/models/stats';
import { Project } from 'src/app/models/project';
import { Router } from '@angular/router';
import { Language } from 'src/app/models/language';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit, OnDestroy {

  sub:Subscription = new Subscription();
  user:User | null=null;
  public visible : boolean = true;
  public inProgress:boolean=false;  

  public timeleft:number=30;
  interval:any;

  public currentUsername:string | null ="";
  public projects: Project[] = [];
  public languages: Language[] = [];
  public statistics:Stats = new Stats();
  public funFacts:Map<string,string> = new Map<string,string>();
  public daysInWeek: string[] = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];

  constructor(private auth: AuthService,private jwt:JwtService, private http:HttpClient,private router: Router) {
  }

  public async getData(username:string):Promise< void>{
    const headers:HttpHeaders = new HttpHeaders().set("Content-Type","application/json").set('Authorization',"Bearer "+this.jwt.getToken());
    const statsResponse:any = await lastValueFrom(this.http.get<any>(environment.backendSecond+"api/repo/getBasicData?username=" + username,{headers,responseType:"json"}));
    this.statistics = new Stats(statsResponse.numberOfRepos,statsResponse.numberOfCommits,statsResponse.numberOfLines );

    const projectsResponse = await lastValueFrom(this.http.get<any>(environment.backendSecond+"api/repo/getRepoObjects?username=" + this.user?.username,{responseType:"json"}));
    
    for(let element of projectsResponse){
      let name: string = element.name;
      let pom = name.lastIndexOf('/');
      name = name.substring(pom +1);
      const description: string = element.description;
      const url: string = element.url;
      const numOfLines: number = element.numOfLines === NaN ? 0:Number(element.numOfLines) ;
      const numOfCommits: number = element.numOfCommits === NaN ? 0:Number(element.numOfCommits) ;
      const languages:string[] = element.languages

      const project = new Project(name,url,description,numOfCommits,numOfLines,languages);
      this.projects.push(project);
    }

    const numberOfDay = await lastValueFrom(this.http.get<any>(environment.backendSecond+"api/repo/getMostProductiveDay?username=" + this.user?.username,{responseType:"json"}));
    const day = this.daysInWeek[numberOfDay-1];
    this.funFacts.set("Most productive day: ",String(day));


    const periodOfDay = await lastValueFrom(this.http.get<any>(environment.backendSecond+"api/repo/getMostProductivePeriod?username=" + this.user?.username,{responseType:"json"}));
    this.funFacts.set("Most productive period of the day: ",periodOfDay);


    const languagesResponse = await lastValueFrom(this.http.get<any>(environment.backendSecond+"api/repo/getLangInfo?username=" + this.user?.username,{responseType:"json"}))
    for(let element in languagesResponse){
      let name: string = element.toUpperCase();
      const numOfLines: number = Number(languagesResponse[element][0]) ;
      const numOfCommits: number = Number(languagesResponse[element][1]);
      const procentage: number = Number((Number(languagesResponse[element][2])).toPrecision(3));
      let imgUrl: string = languagesResponse[element][3];

      const lang = new Language(name,numOfLines,numOfCommits,procentage,imgUrl);
      this.languages.push(lang);
    }

    this.languages = this.languages.sort((a,b) => {
      if(a.getProcentage() > b.getProcentage()){
        return -1;
      }
      if(a.getProcentage() < b.getProcentage()){
        return 1;
      }
      return 0;
    })

    this.languages = this.languages.slice(0,20);
  }

  async ngOnInit(): Promise<void> {

    const url = this.router.url;
    const index = url.lastIndexOf('/');
    const username = url.substring(index+1);
    
    const currentUser: User | null = await lastValueFrom(this.auth.getUser());

    this.currentUsername = currentUser?.username? currentUser.username : null;
    if(this.currentUsername === username ){
      const headers:HttpHeaders = new HttpHeaders().set("Content-Type","application/json").set('Authorization',"Bearer "+this.jwt.getToken());
      const response: any = await lastValueFrom(this.http.get<any>(environment.backendSecond+"api/users?username=" + username,{headers,responseType:"json"}));
      await this.checkInProgress();
      this.inProgress = response.inProgress;
      var usernameResponse = response.username.value;

      var emailResponse:string | null = (response.email === undefined) ? null: response.email.value ;
      var locationResponse:string | null =(response.location === undefined) ? null:response.location.value ;
      var dateResponse:Date | null = (response.dateOfBirth === undefined) ? null:response.dateOfBirth.value ;

      var imgResponse = response.avatar.value;
      if(imgResponse === null || imgResponse === undefined){
        imgResponse = "../../../../assets/user1.png";
      }

      var githuburlResponse = response.githubUrl.value;

      var user= new User(usernameResponse,emailResponse,locationResponse,dateResponse,githuburlResponse,imgResponse);
      this.user = user;

      await this.getData(this.currentUsername);
      this.visible = false;
    }
    else{
      const response:any = await this.getUserResponse(username);
    }
  }

  ngOnDestroy(): void {
      if(this.interval){
        clearInterval(this.interval);
      }
  }

  async getUserResponse(username:string):Promise<void>{
    const response: any = await lastValueFrom(this.http.get<any>(environment.backendSecond+"api/users?username=" + username,{responseType:"json"})
    .pipe(catchError((err:HttpErrorResponse) => {
      if(err.status === 404){
        this.router.navigateByUrl('/404')
        return "" 
      }
      return ""
    }),
    map((response:any) => {

      var usernameResponse = response.username.value;
      var emailResponse:string | null = (response.email === undefined) ? null: response.email.value ;
      var locationResponse:string | null =(response.location === undefined) ? null:response.location.value ;
      var dateResponse:Date | null = (response.dateOfBirth === undefined) ? null:response.dateOfBirth.value ;
      var imgResponse = response.avatar.value;
      var githuburlResponse = response.githubUrl.value;
      var user= new User(usernameResponse,emailResponse,locationResponse,dateResponse,githuburlResponse,imgResponse);
      this.user = user;
      this.getData(usernameResponse);
      this.visible = false;
    })));

    await this.checkInProgress();

  }

  public async checkInProgress(){
    const url = this.router.url;
    const index = url.lastIndexOf('/');
    const username = url.substring(index+1);  
    
    const headers :HttpHeaders = new HttpHeaders().set('content-type', 'application/json').set('Authorization',"Bearer "+this.jwt.getToken());
    const response: any = await lastValueFrom(this.http.get<any>(environment.backendSecond+"api/users?username=" + username, {responseType:"json" }));
  
    this.inProgress = response.inProgress;
   
   if(this.inProgress){
      setTimeout(()=>{this.router.navigateByUrl('/info/'+username).then(() => {
        window.location.reload()
      });},30000);
    
     }else{
      clearInterval(this.interval);
    }
  } 
}
