import { Component, Input, OnInit } from '@angular/core';
import { Language } from 'src/app/models/language';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.css']
})
export class LanguagesComponent implements OnInit {

  @Input() languages:Language[];
  public lang: Language;
  
  constructor() {
    this.languages= [];
    this.lang = new Language("",0,0,0);
  }

  ngOnInit(): void {
    
  }
}
