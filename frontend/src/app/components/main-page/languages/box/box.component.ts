import { Component, Input, OnInit } from '@angular/core';
import { Language } from 'src/app/models/language';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit {
  @Input() lang:Language;

  constructor() {
    this.lang= new Language("",0,0,0);
   }

  ngOnInit(): void {
  }

}
