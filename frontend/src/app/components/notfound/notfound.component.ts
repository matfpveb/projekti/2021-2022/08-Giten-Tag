import { Component, OnInit } from '@angular/core';
import { JwtService } from 'src/app/services/jwt.service';

@Component({
  selector: 'app-notfound',
  templateUrl: './notfound.component.html',
  styleUrls: ['./notfound.component.css']
})
export class NotfoundComponent implements OnInit {

  urlInfo: string = "";
  hasUser: boolean = false;

  constructor(private jwt: JwtService) { }

  ngOnInit(): void {
    if(this.jwt.getToken() !== " "){
      this.urlInfo = "http://localhost:4200/info/" + localStorage.getItem("username");
      this.hasUser = true;
    }
    else {
      this.hasUser = false;
    }
  }
}
