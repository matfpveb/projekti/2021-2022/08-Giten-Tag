import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { lastValueFrom, Subscription } from "rxjs";
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { JwtService } from 'src/app/services/jwt.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit, OnDestroy {
  user:User | null =null;
  userSub:Subscription = new Subscription();
  public userLoggedIn: boolean = false;
  public usernameSearch:string="";

  constructor(private auth:AuthService, private jwt:JwtService, private router: Router) {
  }

  async ngOnInit(){
    this.userSub = this.auth.user.subscribe((user:User|null)=>{
      this.user=user;
      if(user!==null){
        this.userLoggedIn = true;
      }else{
        this.userLoggedIn = false;
      }
    })

    if(this.jwt.getToken() !== ' '){
      this.userSub = this.auth.getUser().subscribe((user:User|null)=>{
        this.user=user;
      })
      this.userLoggedIn = true;
    }else{
      this.userLoggedIn=false;
    }
  }

  ngOnDestroy(){
    this.userSub ? this.userSub.unsubscribe() : null;
  }

  public loginOrLogout():void {
    if(this.user){
      this.auth.logoutUser();
      this.userLoggedIn = this.auth.isUserLoggedIn;
    } else{
      this.userLoggedIn = this.auth.isUserLoggedIn;
    }
  }

  onSearchInput(event:Event){
    this.usernameSearch = (event.target as HTMLInputElement).value;
    this.router.navigateByUrl(`/info/${this.usernameSearch}`)
    .then(() => {
      window.location.reload()
    })
    this.usernameSearch = "";
  }

  goToMainPage(){
    let indeks = (this.router.url.lastIndexOf('/'));
    var urlUsername = this.router.url.substring(indeks+1);
    if(urlUsername=== this.user?.username){
      return;
    }
    this.router.navigateByUrl(`/info/${this.user?.username}`) .then(() => {
        window.location.reload()})
  }
}
