import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { User } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'frontend';
  user : User | null = null;

  constructor(private auth: AuthService){
    this.auth.user.subscribe((user: User | null) => {
        this.user = user;
    })
  }

  ngOnInit(): void {
      
  }
}
