export class User {

    constructor( 
        public username: string | null, 
        public email: string | null, 
        public location:string | null = "",
        public dateOfBirth:Date | null | undefined = undefined,
        public githubUrl: string | null = "",
        public avatar: string | null = ""
      ) {
        }

    public getName():string|null{
        return this.username;
    }

    public getEmail():string|null{
        return this.email;
    }

    public getLocation():string|null{
        return this.location;
    }
    
    public getDateOfBirth():Date|null | undefined{
        return this.dateOfBirth;
    }
    
}
