export class Project {

    constructor(public name: string, 
        public webUrl: string,
        public description : string,
        public numOfCommits: number = 0,
        public numOfLines:number = 0,
        public usedLanguages: string[] = []) {}

    public getName():string{
        return this.name;
    }

    public getWebUrl():string{
        return this.webUrl;
    }

    public getDescription():string{
        return this.description;
    }
}
