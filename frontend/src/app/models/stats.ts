export class Stats{
    constructor(
        public numRepo:number=0,
        public numCommit:number=0,
        public numLines:number=0){
    }

    getNumOfRepos():number|null{
      return this.numRepo;
    }
    getNumOfCommits():number|null{
        return this.numCommit;
      }
      getNumOfLines():number|null{
        return this.numLines;
      }
}