export class Language {

    constructor(public name: string,
        public numOfLines: number,
        public numOfCommits: number, 
        public procentage: number, 
        public imgUrl: string = "") {}

    public getName():string{
        return this.name;
    }

    public getProcentage():number{
        return this.procentage;
    }
    
    public getImgUrl():string{
        return this.imgUrl;
    }
}